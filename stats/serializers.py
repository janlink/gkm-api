from rest_framework import serializers
from stats.models import ChartPosistion

class ChartPosistionSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChartPosistion
        fields = ['id', 'id_game', 'source', 'position']