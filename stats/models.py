from django.db import models

class ChartPosistion(models.Model):
    TYPE = (
        ('steam', 'Steam charts'),
        ('igdb', 'IGDB'),
        ('amazon', 'Amazon Charts'),
        ('n/a', 'Not available')
    )
    id_game = models.fields.IntegerField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    source = models.CharField(max_length=25, choices=TYPE, default='n/a')
    position = models.fields.IntegerField()