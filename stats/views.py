
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.decorators import action
from stats.models import ChartPosistion
from stats.serializers import ChartPosistionSerializer

class ChartPosistionViewSet(viewsets.ModelViewSet):
    """
    API endpoint returns all infos.
    """
    queryset = ChartPosistion.objects.all()
    serializer_class = ChartPosistionSerializer
    permission_classes = [permissions.AllowAny]
    filterset_fields = ['id_game', 'source']

    @action(detail=False, methods=['get'])
    def delete_all(self, request):
        ChartPosistion.objects.all().delete()
        return Response('success')