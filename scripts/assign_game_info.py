import django
from django.core.exceptions import ObjectDoesNotExist
import os
import sys
import re
import json
from pathlib import Path
import logging
from logging.handlers import RotatingFileHandler

os.environ['DJANGO_SETTINGS_MODULE'] = 'gkm-api.settings'
BASE_DIR = os.path.abspath(os.getcwd())

sys.path.insert(0, os.path.join(BASE_DIR, "staging"))
sys.path.insert(0, os.path.join(BASE_DIR, "gamedata"))
sys.path.insert(0, BASE_DIR)

django.setup()

from staging.models import IgdbGenre, IgdbInvolvedCompanies, IgdbArtwork, IgdbScreenshot, IgdbCover, IgdbExternalGame, IgdbGame, IgdbCompany, IgdbVideo, SteamGame
from gamedata.models import Game, GameDescription, Screenshot, Shop, Deal, Price, Info

# create logger
logger = logging.getLogger('AssignInfo')
logging.basicConfig(level=logging.DEBUG)
# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create file handler
# fh = RotatingFileHandler(filename='../logs/cron_assign_game_to_gameentry.log', maxBytes=5242880)
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# add formatter to ch
ch.setFormatter(formatter)
# fh.setFormatter(formatter)
# add ch to logger
logger.addHandler(ch)
# logger.addHandler(fh)

class InfoExtractor():
    def check_key_exist(key, dict_to_check):
        return True if key in dict_to_check else False

    def extract_igdb_info(self, json_dict):
        if json:
            json_data = json.dumps(json_dict)
            if self.check_key_exist('summary', json_data):
                print(json_data)

    def extract_steam_info(json_dict):
        # print(json)
        pass

    # def extract_key_from_json(self, json_dict, key):
        


all_games = Game.objects.filter(id=61811)
all_igdb_games = IgdbGame.objects.all()
all_steam_games = SteamGame.objects.all()

for game in all_games:
    print(game.name)

    infos = Info.objects.filter(fk_game=game)
    for info in infos:
        igdb_info = None
        steam_info = None
        try:
            if info.source == 'igdb':
                igdb_info = IgdbGame.objects.get(id_ext=info.source_id)
                InfoExtractor.extract_igdb_info(json_dict=igdb_info.dyn_json)
            if info.source == 'steam':
                steam_info = SteamGame.objects.get(id_ext=info.source_id)
                InfoExtractor.extract_steam_info(steam_info.dyn_json)
        except ObjectDoesNotExist as e:
            logger.exception(e, exc_info=False)
            
        # print(igdb_info)
        # print(steam_info)

