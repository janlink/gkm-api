import os
import re
import sys
import django
os.environ['DJANGO_SETTINGS_MODULE'] = 'gkm-api.settings'
BASE_DIR = os.path.abspath(os.getcwd())

sys.path.insert(0, os.path.join(BASE_DIR, "staging"))
sys.path.insert(0, os.path.join(BASE_DIR, "gamedata"))
sys.path.insert(0, BASE_DIR)

django.setup()
from gamedata.models import Game, Deal
from helper.string_helper import clean_keyword

# keyword = 'destiny - the collection xbox live key xbox one europe'
# print(clean_keyword(keyword))

# regex = '(\sxbox(((\s|$)|(\sxbox)|(one|live|360)))*(\s|$))'
# namesRegex = re.compile(fr"{regex}", re.I)
# replaced = namesRegex.sub(r" ", keyword.lower())
# print(replaced)

for item in Deal.objects.filter(fk_game_id__isnull=True).filter(name__icontains = 'collection')[:1000]:
    print('{} ////// {}'.format(clean_keyword(item.name), item.name.lower()))