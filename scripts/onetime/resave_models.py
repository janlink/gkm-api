import os
import sys
import django
os.environ['DJANGO_SETTINGS_MODULE'] = 'gkm-api.settings'
BASE_DIR = os.path.abspath(os.getcwd())

sys.path.insert(0, os.path.join(BASE_DIR, "staging"))
sys.path.insert(0, os.path.join(BASE_DIR, "gamedata"))
sys.path.insert(0, BASE_DIR)

django.setup()

from gamedata.models import Game, Deal
from staging.models import IgdbGame, SteamGame

for item in Game.objects.all():
    item.save()

for item in Deal.objects.all():
    item.save()

for item in IgdbGame.objects.all():
    item.save()
