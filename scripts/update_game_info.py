import django
import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'gkm-api.settings'
BASE_DIR = os.path.abspath(os.getcwd())

sys.path.insert(0, os.path.join(BASE_DIR, "staging"))
sys.path.insert(0, os.path.join(BASE_DIR, "gamedata"))
sys.path.insert(0, BASE_DIR)

django.setup()

from helper.logger_factory import LoggerFactory
from gamedata.models import Game, GameDescription, Screenshot, Shop, Deal, Price, Info
from staging.models import IgdbGenre, IgdbInvolvedCompanies, IgdbArtwork, IgdbScreenshot, IgdbCover, IgdbExternalGame, IgdbGame, IgdbCompany, IgdbVideo, SteamGame

    # Desc:     link staging info to Game with info
    # Input:    staging_model - staging.model instance
    # Returns:  gamedata.Game instance
    # Throws:   None