import django
import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'gkm-api.settings'
BASE_DIR = os.path.abspath(os.getcwd())

sys.path.insert(0, os.path.join(BASE_DIR, "staging"))
sys.path.insert(0, os.path.join(BASE_DIR, "gamedata"))
sys.path.insert(0, BASE_DIR)

django.setup()

from staging.models import IgdbGenre, IgdbInvolvedCompanies, IgdbArtwork, IgdbScreenshot, IgdbCover, IgdbExternalGame, IgdbGame, IgdbCompany, IgdbVideo, SteamGame
from gamedata.models import Game, GameDescription, Screenshot, Shop, Deal, Price, Info
from helper.logger_factory import LoggerFactory

class Deal2Game():
    def __init__(self):
        self.logger = LoggerFactory().create('Deal2Game', 'debug')

    # Desc:     This function returns am gamedata.Game instance.
    #           If the game is not in database, a new one is created.
    # Input:    staging_model - staging.model instance
    # Returns:  gamedata.Game instance
    # Throws:   None
    def get_or_create_game(staging_model):
        game = Game.objects.filter(
            cleaned_name=staging_model.cleaned_name).first()
        if game:
            return game
        else:
            g = Game(name=staging_model.name)
            g.save()
            return g

    # Desc:     This function returns am gamedata.Info instance.
    #           If the info is not in database, a new one is created.
    # Input:    game_id - The id of the gamedata.Game instance
    #           source - The source field of the info entry (steam or igdb)
    #           model_instance - staging.model instance
    # Returns:  gamedata.Info instance
    # Throws:   None
    def get_or_create_info(game_id, source, model_instance):
        info = Info.objects.filter(fk_game__id=game_id)\
            .filter(source_id=model_instance.id_ext)\
            .filter(source=source)\
            .first()
        if info:
            return info
        else:
            game = Game.objects.get(pk=game_id)
            info = Info(
                fk_game=game, source_id=model_instance.id_ext, source=source)
            info.save()
            return info

    def assing_deals(self):
        # all_deals = Deal.objects.filter(is_checked=False)
        all_deals = Deal.objects.filter(fk_game_id__isnull=True)
        all_games = Game.objects.all()
        all_igdb_games = IgdbGame.objects.all()
        all_steam_games = SteamGame.objects.all()
        games = []

        count_matches = 0
        count_no_matches = 0
        count_matches_igdb = 0
        count_no_matches_igdb = 0
        count_matches_steam = 0
        count_no_matches_steam = 0

        self.logger.info(
            'No. of unchecked deals: {}'.format(all_deals.count()))

        for deal in all_deals[:100]:
            game = Game.objects.filter(cleaned_name=deal.cleaned_name).first()
            if game:
                count_matches += 1
            else:
                print('{} ////// {}'.format(deal.cleaned_name, deal.name.lower()))
                game_igdb = IgdbGame.objects.filter(
                    cleaned_name=deal.cleaned_name).first()
                if game_igdb:
                    game = Deal2Game.get_or_create_game(game_igdb)
                    # info = Deal2Game.get_or_create_info(game.id, 'igdb', game_igdb)
                    count_matches_igdb += 1
                else:
                    count_no_matches_igdb += 1

                game_steam = SteamGame.objects.filter(
                    cleaned_name=deal.cleaned_name).first()
                if game_steam:
                    game = Deal2Game.get_or_create_game(game_steam)
                    # info_igdb = Deal2Game.get_or_create_info(game.id, 'steam', game_steam)
                    # info_steam = Deal2Game.get_or_create_info(game.id, 'steam', game_steam)
                    count_matches_steam += 1
                else:
                    count_no_matches_steam += 1

            if game:
                deal.fk_game = game
            else:
                count_no_matches += 1

            # deal.is_checked = True
            # deal.save()

        self.logger.info('Matches with games in DB: {}'.format(count_matches))
        self.logger.info('Matches IGDB API: {}'.format(count_matches_igdb))
        self.logger.info('Matches Steam API: {}'.format(count_matches_steam))
        self.logger.info('No Matches: {}'.format(count_no_matches))


Deal2Game().assing_deals()
