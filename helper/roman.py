import re

def roman_to_int(n):
    # n = n.decode("utf-8", "strict").upper()
    numeral_map = zip(
        (10, 9, 5, 4, 1),
        ('X', 'IX', 'V', 'IV', 'I')
    )
    i = result = 0
    for integer, numeral in numeral_map:
        while n[i:i + len(numeral)] == numeral:
            result += integer
            i += len(numeral)
    return result

def roman_to_int_repl(match):
    return str(roman_to_int(match.group(0)))

def replace_roman_numbers_in_string(string):
    s = string
    regex = re.compile(r'\b(?=[MDCLXVI]+\b)M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})\b')
    return regex.sub(roman_to_int_repl, s)
