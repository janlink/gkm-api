import re
import helper.roman

# import math


def multireplace(string, replacements):
    """
    Given a string and a replacement map, it returns the replaced string.

    :param str string: string to execute replacements on
    :param dict replacements: replacement dictionary {value to find: value to replace}
    :rtype: str

    """
    # Place longer ones first to keep shorter substrings from matching where the longer ones should take place
    # For instance given the replacements {'ab': 'AB', 'abc': 'ABC'} against the string 'hey abc', it should produce
    # 'hey ABC' and not 'hey ABc'
    substrs = sorted(replacements, key=len, reverse=True)

    # Create a big OR regex that matches any of the substrings to replace
    regexp = re.compile('|'.join(map(re.escape, substrs)))

    # For each match, look up the new string in the replacements
    return regexp.sub(lambda match: replacements[match.group(0)], string)


def replace_umlauts(string):
    return multireplace(string, {u'ä': 'ae', u'ö': 'oe', u'ü': 'ue'})


def clean_with_substitute(string, regex_list):
    for regex in regex_list:
        string = re.sub(regex, r" ", string.strip())
    return string


def multireplace_keyword(keyword):
    return multireplace(keyword,
                        {
                            '\'': '', '\"': '', '-': ' ', ':': ' ', '[': ' ', ']': ' ', '%': ' ', '.': '', '’': ' ',
                            '\\': ' ', '/': ' ', '~': ' ', '!': ' ', ';': ' ', '–': '', ',': '', '?': '',
                            '(': '',')': '','+': '',
                            u"\u00A9": ' ', u"\u2122": ' ', u"\u00AE": ' '
                        }
                        )


def cut_deal_title(keyword):
    if re.match(r'(.*)(\s\+\s)', keyword):
        keyword = re.match(r'(.*)(\s\+\s)', keyword).group(1)
    if re.match(r'(.*)(\(.*\))', keyword):
        keyword = re.match(r'(.*)(\(.*\))', keyword).group(1)
    if re.match(r'(.*)(steam key)', keyword):
        keyword = re.match(r'(.*)(steam key)', keyword).group(1)

    return keyword


def clean_keyword(keyword):
    regex_list = [
        r'(game of the year edition)',
        r'((\w+|day one) edition)',
        r'(\scollectors)',
        r'((\s|complete|master)*\scollection)',
        r'(\supgrade)',
        r'(\w+ code)',
        r'(\w+ bundle)',
        r'(\w+\spass\s\d)|(\w+\spass)',

        # Additional info in deal title
        r'(\sprepurchase(\s|$))', r'(\spreorder(\s|$))', r'(\sdeluxe(\s|$))', r'(\spremium(\s|$))', r'(\sdigital(\s|$))',
        r'(\ssoundtrack(\s|$))', r'(\sbonus(\s|$))', r'(\sdlc(\s|$))', r'(\skey(\s|$))', r'(\scd(\s|$))', r'(\scdkey(\s|$))',
        r'(\suncut(\s|$))', '(\sgift(\s|$))',

        # System specific
        r'(\sps4(\s|$))', r'(\spsn(\s|$))', r'(playstation 4)',
        r'(\sxbox(((\s|$)|(\sxbox)|(one|live|360)))*(\s|$))',
        r'(\ssteam(((\s|$)|(\s|$)(gift|account)))*(\s|$))',
        r'(\sswitch(\s|$))', '(nintendo switch)',
        r'(\suplay(\s|$))',
        r'(\sgogcom(\s|$))',
        r'(windows 10)', r'(\sorigin(\s|$))', r'(\spc(\s|$))', r'(epic games store)', r'(\sgog(\s|$))',

        # Coins & Points
        r'(\d+\s(points|coins|gold|pack))|(\d+\s\w+\s(points|coins|gold|pack))',

        # Region specific
        r'(\seu(\s|$))', r'(\seurope(\s|$))',
        r'(\sus(\s|$))', r'(\sna(\s|$))', r'(\snorth america)', r'(\sunited states)',
        r'(\suk(\s|$))', r'(\seng(\s|$))', r'(\sen(\s|$))', r'(\sunited kingdom)',
        r'(\spl(\s|$))', r'(\spoland)',
        r'(\semea(\s|$))',
        r'(\sglobal(\s|$))',
        r'(\sru(\s|$))', r'(\scis(\s|$))',
        r'(\saustralia)',
        r'(\slatam)', r'(\sbrazil)',
        r'(\sgermany)',

        # Eliminate more than one whitspace
        '([ ]{2,})',
    ]

    return clean_with_substitute(
        helper.roman.replace_roman_numbers_in_string(
            multireplace_keyword(
                replace_umlauts(
                    cut_deal_title(
                        keyword.lower() + ' '
                    )
                )
            )
        ), regex_list).strip()
