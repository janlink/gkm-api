
import logging
from logging.handlers import RotatingFileHandler

class LoggerFactory:
    levels = {
        'DEBUG': logging.DEBUG,
        'INFO': logging.INFO,
        'WARNING': logging.WARNING,
        'ERROR': logging.ERROR,
        'CRITICAL': logging.CRITICAL
    }

    def create(self, name, level):
        level = self.levels.get(level.upper(), logging.INFO)
        # create logger
        logger = logging.getLogger(name)
        logging.basicConfig(level=level)
        # create console handler and set level to debug
        ch = logging.StreamHandler()
        ch.setLevel(level)
        # create formatter
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        # add formatter to ch
        ch.setFormatter(formatter)
        # add ch to logger
        # logger.addHandler(ch)
        return logger