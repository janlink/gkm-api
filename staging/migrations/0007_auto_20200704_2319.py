# Generated by Django 3.0.5 on 2020-07-04 21:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('staging', '0006_igdbexternalgame'),
    ]

    operations = [
        migrations.AlterField(
            model_name='igdbexternalgame',
            name='name',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='igdbexternalgame',
            name='url',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
