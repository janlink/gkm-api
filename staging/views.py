from django.shortcuts import render
from django.db.models import Q
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework import filters
from staging.models import IgdbGenre, IgdbInvolvedCompanies, IgdbArtwork, IgdbScreenshot, IgdbCover, IgdbExternalGame, IgdbGame, IgdbCompany, IgdbVideo, SteamGame
from staging.serializers import IgdbGenreSerializer, IgdbInvolvedCompaniesSerializer, IgdbArtworkSerializer, IgdbScreenshotSerializer, IgdbScreenshotSerializer, IgdbCoverSerializer, IgdbExternalGameSerializer, IgdbGameSerializer, IgdbCompanySerializer, IgdbVideoSerializer, SteamGameSerializer

# Create your views here.


class GenericQueryset():
    def get_queryset(self, model_instance):
        queryset = model_instance.objects.all()

        id_ext = self.request.query_params.get('id_ext', None)
        if id_ext is not None:
            queryset = queryset.filter(id_ext=id_ext)

        id_ext_list = self.request.query_params.get('id_ext_list', None)
        if id_ext_list is not None:
            queryset = queryset.filter(id_ext__in=eval(id_ext_list))

        return queryset

class IgdbGenreViewSet(viewsets.ModelViewSet):
    """
    API endpoint returns all IgdbGenre.
    """
    serializer_class = IgdbGenreSerializer
    permission_classes = [permissions.AllowAny]

    def get_queryset(self):
        queryset = IgdbGenre.objects.all()

        id_ext = self.request.query_params.get('id_ext', None)
        if id_ext is not None:
            queryset = queryset.filter(id_ext=id_ext)

        id_ext_list = self.request.query_params.get('id_ext_list', None)
        if id_ext_list is not None:
            queryset = queryset.filter(id_ext__in=eval(id_ext_list))

        return queryset

class IgdbInvolvedCompaniesViewSet(viewsets.ModelViewSet):
    """
    API endpoint returns all IgdbInvolvedCompanies.
    """
    serializer_class = IgdbInvolvedCompaniesSerializer
    permission_classes = [permissions.AllowAny]

    def get_queryset(self):
        queryset = IgdbInvolvedCompanies.objects.all()

        id_ext = self.request.query_params.get('id_ext', None)
        if id_ext is not None:
            queryset = queryset.filter(id_ext=id_ext)

        id_ext_list = self.request.query_params.get('id_ext_list', None)
        if id_ext_list is not None:
            queryset = queryset.filter(id_ext__in=eval(id_ext_list))

        return queryset

class IgdbArtworkViewSet(viewsets.ModelViewSet):
    """
    API endpoint returns all IgdbArtwork.
    """
    serializer_class = IgdbArtworkSerializer
    permission_classes = [permissions.AllowAny]

    def get_queryset(self):
        queryset = IgdbArtwork.objects.all()

        id_ext = self.request.query_params.get('id_ext', None)
        if id_ext is not None:
            queryset = queryset.filter(id_ext=id_ext)

        id_ext_list = self.request.query_params.get('id_ext_list', None)
        if id_ext_list is not None:
            queryset = queryset.filter(id_ext__in=eval(id_ext_list))

        return queryset

class IgdbScreenshotViewSet(viewsets.ModelViewSet):
    """
    API endpoint returns all IgdbScreenshot.
    """
    serializer_class = IgdbScreenshotSerializer
    permission_classes = [permissions.AllowAny]

    def get_queryset(self):
        queryset = IgdbScreenshot.objects.all()

        id_ext = self.request.query_params.get('id_ext', None)
        if id_ext is not None:
            queryset = queryset.filter(id_ext=id_ext)

        id_ext_list = self.request.query_params.get('id_ext_list', None)
        if id_ext_list is not None:
            queryset = queryset.filter(id_ext__in=eval(id_ext_list))

        return queryset

class IgdbCoverViewSet(viewsets.ModelViewSet):
    """
    API endpoint returns all IgdbScreenshot.
    """
    serializer_class = IgdbCoverSerializer
    permission_classes = [permissions.AllowAny]

    def get_queryset(self):
        queryset = IgdbCover.objects.all()

        id_ext = self.request.query_params.get('id_ext', None)
        if id_ext is not None:
            queryset = queryset.filter(id_ext=id_ext)

        id_ext_list = self.request.query_params.get('id_ext_list', None)
        if id_ext_list is not None:
            queryset = queryset.filter(id_ext__in=eval(id_ext_list))

        return queryset

class IgdbExternalGameViewSet(viewsets.ModelViewSet):
    """
    API endpoint returns all IgdbExternalGame.
    """
    serializer_class = IgdbExternalGameSerializer
    permission_classes = [permissions.AllowAny]

    def get_queryset(self):
        queryset = IgdbExternalGame.objects.all()

        id_ext = self.request.query_params.get('id_ext', None)
        if id_ext is not None:
            queryset = queryset.filter(id_ext=id_ext)

        id_ext_list = self.request.query_params.get('id_ext_list', None)
        if id_ext_list is not None:
            queryset = queryset.filter(id_ext__in=eval(id_ext_list))

        return queryset

class IgdbGameViewSet(viewsets.ModelViewSet):
    """
    API endpoint returns all IgdbGame.
    """
    serializer_class = IgdbGameSerializer
    permission_classes = [permissions.AllowAny]

    def get_queryset(self):
        queryset = IgdbGame.objects.all()

        id_ext = self.request.query_params.get('id_ext', None)
        if id_ext is not None:
            queryset = queryset.filter(id_ext=id_ext)

        id_ext_list = self.request.query_params.get('id_ext_list', None)
        if id_ext_list is not None:
            queryset = queryset.filter(id_ext__in=eval(id_ext_list))

        return queryset

class IgdbCompanyViewSet(viewsets.ModelViewSet):
    """
    API endpoint returns all IgdbCompany.
    """
    serializer_class = IgdbCompanySerializer
    permission_classes = [permissions.AllowAny]

    def get_queryset(self):
        queryset = IgdbCompany.objects.all()

        id_ext = self.request.query_params.get('id_ext', None)
        if id_ext is not None:
            queryset = queryset.filter(id_ext=id_ext)

        id_ext_list = self.request.query_params.get('id_ext_list', None)
        if id_ext_list is not None:
            queryset = queryset.filter(id_ext__in=eval(id_ext_list))

        return queryset

class IgdbVideoViewSet(viewsets.ModelViewSet):
    """
    API endpoint returns all IgdbCompany.
    """
    serializer_class = IgdbVideoSerializer
    permission_classes = [permissions.AllowAny]

    def get_queryset(self):
        queryset = IgdbVideo.objects.all()

        id_ext = self.request.query_params.get('id_ext', None)
        if id_ext is not None:
            queryset = queryset.filter(id_ext=id_ext)

        id_ext_list = self.request.query_params.get('id_ext_list', None)
        if id_ext_list is not None:
            queryset = queryset.filter(id_ext__in=eval(id_ext_list))

        return queryset

class SteamGameViewSet(viewsets.ModelViewSet):
    """
    API endpoint returns all SteamGame.
    """
    serializer_class = SteamGameSerializer
    permission_classes = [permissions.AllowAny]

    def get_queryset(self):
        queryset = SteamGame.objects.all()

        id_ext = self.request.query_params.get('id_ext', None)
        if id_ext is not None:
            queryset = queryset.filter(id_ext=id_ext)

        id_ext_list = self.request.query_params.get('id_ext_list', None)
        if id_ext_list is not None:
            queryset = queryset.filter(id_ext__in=eval(id_ext_list))
        
        dyn_json = self.request.query_params.get('dyn_json', None)
        if dyn_json is not None:
            if dyn_json == 'null':
                queryset = queryset.filter(Q(dyn_json__isnull=True) | Q(dyn_json=[]))
            else:
                queryset = queryset.filter(dyn_json=dyn_json)

        return queryset