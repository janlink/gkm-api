from rest_framework import serializers
from staging.models import IgdbGenre, IgdbInvolvedCompanies, IgdbArtwork, IgdbScreenshot, IgdbCover, IgdbExternalGame, IgdbGame, IgdbCompany, IgdbVideo, SteamGame


class IgdbGenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = IgdbGenre
        fields = ['created', 'updated', 'id', 'id_ext', 'updated_at', 'name']


class IgdbInvolvedCompaniesSerializer(serializers.ModelSerializer):
    class Meta:
        model = IgdbInvolvedCompanies
        fields = ['created', 'updated', 'id', 'id_ext', 'updated_at', 'company', 'developer',
                  'porting', 'publisher', 'supporting']


class IgdbArtworkSerializer(serializers.ModelSerializer):
    class Meta:
        model = IgdbArtwork
        fields = ['created', 'updated', 'id', 'id_ext', 'game', 'height', 'image_id',
                  'url', 'width']


class IgdbScreenshotSerializer(serializers.ModelSerializer):
    class Meta:
        model = IgdbScreenshot
        fields = ['created', 'updated', 'id', 'id_ext', 'game', 'height', 'image_id',
                  'url', 'width']


class IgdbCoverSerializer(serializers.ModelSerializer):
    class Meta:
        model = IgdbCover
        fields = ['created', 'updated', 'id', 'id_ext', 'game', 'height', 'image_id',
                  'url', 'width']


class IgdbExternalGameSerializer(serializers.ModelSerializer):
    class Meta:
        model = IgdbExternalGame
        fields = ['created', 'updated', 'id', 'id_ext', 'updated_at',
                  'category', 'game', 'name', 'uid', 'url']


class IgdbGameSerializer(serializers.ModelSerializer):
    class Meta:
        model = IgdbGame
        fields = ['created', 'updated', 'id', 'id_ext', 'updated_at', 'name', 'cleaned_name', 'dyn_json']


class IgdbCompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = IgdbCompany
        fields = ['created', 'updated', 'id', 'id_ext', 'updated_at',  'country',
                  'description', 'name', 'url', 'websites']


class IgdbVideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = IgdbVideo
        fields = ['created', 'updated', 'id', 'id_ext', 'game', 'name', 'video_id']

class SteamGameSerializer(serializers.ModelSerializer):
    class Meta:
        model = SteamGame
        fields = ['created', 'updated', 'id', 'id_ext', 'updated_at', 'name', 'dyn_json']