from django.db import models
from django.contrib.postgres.fields import JSONField
from helper.string_helper import clean_keyword


class IgdbGenre(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    id_ext = models.fields.IntegerField()
    updated_at = models.fields.IntegerField()
    name = models.CharField(max_length=255)


class IgdbInvolvedCompanies(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    id_ext = models.fields.IntegerField()
    updated_at = models.fields.IntegerField()
    company = models.CharField(max_length=255)
    developer = models.BooleanField(default=False)
    porting = models.BooleanField(default=False)
    publisher = models.BooleanField(default=False)
    supporting = models.BooleanField(default=False)


class IgdbArtwork(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    id_ext = models.fields.IntegerField()
    game = models.fields.IntegerField()
    height = models.fields.IntegerField()
    image_id = models.CharField(max_length=255)
    url = models.CharField(max_length=255)
    width = models.fields.IntegerField()


class IgdbScreenshot(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    id_ext = models.fields.IntegerField()
    game = models.fields.IntegerField()
    height = models.fields.IntegerField(blank=True, null=True)
    image_id = models.CharField(max_length=255)
    url = models.CharField(max_length=255)
    width = models.fields.IntegerField(blank=True, null=True)


class IgdbCover(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    id_ext = models.fields.IntegerField()
    game = models.fields.IntegerField()
    height = models.fields.IntegerField()
    image_id = models.CharField(max_length=255)
    url = models.CharField(max_length=255)
    width = models.fields.IntegerField()


class IgdbExternalGame(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    id_ext = models.fields.IntegerField()
    updated_at = models.fields.IntegerField()
    category = models.fields.IntegerField()
    game = models.fields.IntegerField()
    name = models.CharField(max_length=255, blank=True, null=True)
    uid = models.CharField(max_length=255)
    url = models.CharField(max_length=255, blank=True, null=True)


class IgdbGame(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    id_ext = models.fields.IntegerField()
    updated_at = models.fields.IntegerField()
    name = models.CharField(max_length=255)
    cleaned_name = models.CharField(max_length=255, blank=True, null=True)
    dyn_json = JSONField(models.CharField(
        max_length=255, blank=True, null=True), default=list, null=True)
    
    def save(self, *args, **kwargs):
        self.cleaned_name = clean_keyword(self.name)
        super(IgdbGame, self).save(*args, **kwargs)


class IgdbCompany(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    id_ext = models.fields.IntegerField()
    updated_at = models.fields.IntegerField()
    country = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    name = models.CharField(max_length=255)
    url = models.CharField(max_length=255, blank=True, null=True)
    websites = JSONField(models.CharField(
        blank=True, null=True), default=list, null=True)


class IgdbVideo(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    id_ext = models.fields.IntegerField()
    game = models.fields.IntegerField()
    name = models.CharField(max_length=255)
    video_id = models.CharField(max_length=255)

class SteamGame(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    id_ext = models.fields.IntegerField()
    updated_at = models.fields.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    cleaned_name = models.CharField(max_length=255, blank=True, null=True)
    dyn_json = JSONField(models.CharField(
        max_length=255, blank=True, null=True), default=list, null=True)
    
    def save(self, *args, **kwargs):
        self.cleaned_name = clean_keyword(self.name)
        super(SteamGame, self).save(*args, **kwargs)