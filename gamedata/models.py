from django.db import models
from django.db.models import F, Value
from django.template.defaultfilters import slugify
from django.contrib.postgres.fields import ArrayField, JSONField
from helper.string_helper import clean_keyword

class Game(models.Model):
    GAME_TYPE = (
        ('game', 'Base game without DLC'),
        ('dlc', 'DLC only'),
        ('card', 'Card / Credits'),
        ('software', 'Software'),
        ('other', 'Other type')
    )

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=255)
    cleaned_name = models.CharField(max_length=255, blank=True, null=True)
    slug = models.SlugField(default='no-slug', max_length=60, blank=True)
    gtype = models.CharField(max_length=25, choices=GAME_TYPE, default='other')
    cover_horizontal = models.URLField(max_length=550, blank=True, null=True)
    cover_vertical = models.URLField(max_length=550, blank=True, null=True)
    artwork = models.URLField(max_length=550, blank=True, null=True)
    release_date = models.DateField(blank=True, null=True)
    developer = models.CharField(max_length=255, blank=True, null=True)
    publisher = models.CharField(max_length=255, blank=True, null=True)
    website = models.URLField(max_length=550, blank=True, null=True)
    genre = ArrayField(models.CharField(
        max_length=255, blank=True, null=True), default=list)

    def save(self, *args, **kwargs):
        # save a slug if there is no slug or when it's 'no-slug' (the default slug)
        if not self.slug or self.slug == 'no-slug':
            # Or whatever you want the slug to use
            self.slug = slugify(self.name)[:60]
        self.cleaned_name = clean_keyword(self.name)

        super(Game, self).save(*args, **kwargs)

class GameDescription(models.Model):
    LANGUAGES = (
        ('de', 'Deutsch'),
        ('en', 'English'),
        ('fr', 'Francais'),
        ('es', 'Espanol'),
        ('pl', 'Polski')
    )

    fk_game = models.ForeignKey(
        Game,
        models.CASCADE
    )
    language = models.CharField(max_length=25, choices=LANGUAGES)
    desc_short = models.TextField(blank=True, null=True)
    desc_long = models.TextField(blank=True, null=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['fk_game', 'language'], name='unique_game_lang')
        ]


class Screenshot(models.Model):
    fk_game = models.ForeignKey(
        Game,
        models.CASCADE
    )
    image = models.URLField(max_length=550, blank=True, null=True)


class Shop(models.Model):
    STATUS = (
        ('active', 'Active'),
        ('inactive', 'Inactive')
    )
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(
        max_length=25, choices=STATUS, default='inactive')
    name = models.CharField(max_length=255)
    logo = models.ImageField(
        upload_to='logos', blank=True, null=True, max_length=550)
    shop_url = models.URLField(max_length=550, blank=True, null=True)
    affiliate_url = models.URLField(max_length=550, blank=True, null=True)
    partner_link = models.CharField(max_length=255, blank=True, null=True)
    trustpilot_url = models.URLField(max_length=550, blank=True, null=True)
    trustpilot_rating = models.IntegerField(blank=True, null=True)

class Deal(models.Model):
    STATUS = (
        ('available', 'Available'),
        ('preorder', 'Preorder'),
        ('n/a', 'Not available'),
    )

    PLATTFORM = (
        ('steam', 'Steam'),
        ('origin', 'Origin'),
        ('uplay', 'UPlay'),
        ('battlenet', 'Battle.net'),
        ('windowsstore', 'Windows Store'),
        ('gog', 'GOG'),
        ('rockstar', 'Rockstar Social Club'),
        ('drmfree', 'DRM free'),
        ('bethesda', 'Bethesda.net Launcher'),
        ('squareenix', 'Square Enix Launcher'),
        ('epic', 'Epic Game Store'),
        ('playstation', 'Playstation Store'),
        ('xbox', 'Xbox Store'),
        ('xboxplayanywhere', 'Xbox Play Anywhere'),
        ('nintendo', 'Nintendo eStore'),
        ('n/a', 'Not available')
    )

    REGION = (
        ('ger', 'Germany'),
        ('eu', 'Europe'),
        ('uk', 'United Kingdom'),
        ('us', 'United States'),
        ('ru', 'Russia'),
        ('pl', 'Poland'),
        ('apac', 'Asia/Pacific'),
        ('emea', 'Europe, Middle East and Africa'),
        ('latam', 'Latin America'),
        ('world', 'Worldwide'),
        ('n/a', 'Not available')
    )

    TYPE = (
        ('game', 'Base game without DLC'),
        ('dlc', 'DLC only'),
        ('credits', 'Card / Credits'),
        ('n/a', 'Not available')
    )

    # internal fields
    fk_game = models.ForeignKey(
        Game,
        models.CASCADE,
        blank=True,
        null=True
    )
    fk_shop = models.ForeignKey(
        Shop,
        models.CASCADE
    )
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    # process related fields
    is_checked = models.BooleanField(default=False)

    # display data fields
    url = models.URLField(max_length=550, blank=True, null=True)
    name = models.CharField(max_length=255)
    cleaned_name = models.CharField(max_length=255, blank=True, null=True)
    tpye = models.CharField(max_length=25, choices=TYPE, default='n/a')
    status = models.CharField(max_length=25, choices=STATUS, default='n/a')
    plattform = models.CharField(
        max_length=25, choices=PLATTFORM, default='n/a')
    region = models.CharField(max_length=25, choices=REGION, default='n/a')

    def save(self, *args, **kwargs):
        self.cleaned_name = clean_keyword(self.name)
        super(Deal, self).save(*args, **kwargs)

class Price(models.Model):
    CURENCY = (
        ('eur', 'Euro'),
        ('usd', 'US Dollar'),
        ('gbp', 'Pound sterling'),
        ('n/a', 'Not available')
    )

    fk_deal = models.ForeignKey(
        Deal,
        models.CASCADE
    )
    created = models.DateTimeField(auto_now_add=True)
    currency = models.CharField(max_length=3, choices=CURENCY, default='n/a')
    amount = models.DecimalField(max_digits=5, decimal_places=2, default=0.0)

class Info(models.Model):
    SOURCE = (
        ('steam', 'Steam'),
        ('igdb', 'IGDB.com'),
        ('n/a', 'Not available')
    )
    fk_game = models.ForeignKey(
        Game,
        models.CASCADE
    )
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    source = models.CharField(max_length=25, choices=SOURCE, default='n/a')
    source_id = models.CharField(max_length=255)
    api_url = models.URLField(max_length=550, blank=True, null=True)
    dyn_json = JSONField(models.CharField(
        max_length=255, blank=True, null=True), default=list, null=True)
