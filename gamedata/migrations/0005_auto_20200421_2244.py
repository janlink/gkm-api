# Generated by Django 3.0.5 on 2020-04-21 20:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gamedata', '0004_auto_20200416_2307'),
    ]

    operations = [
        migrations.AlterField(
            model_name='game',
            name='cover',
            field=models.ImageField(blank=True, max_length=550, null=True, upload_to='cover'),
        ),
        migrations.AlterField(
            model_name='screenshot',
            name='image',
            field=models.ImageField(max_length=550, upload_to='screenshots'),
        ),
    ]
