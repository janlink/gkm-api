# Generated by Django 3.0.5 on 2020-04-16 21:07

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gamedata', '0003_auto_20200408_2327'),
    ]

    operations = [
        migrations.CreateModel(
            name='Deal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=255)),
                ('status', models.CharField(choices=[('available', 'Available'), ('n/a', 'Not available'), ('preorder', 'Preorder')], default='notavailable', max_length=25)),
                ('plattform', models.CharField(choices=[('steam', 'Steam'), ('origin', 'Origin'), ('uplay', 'UPlay'), ('battlenet', 'Battle.net'), ('windowsstore', 'Windows Store'), ('gog', 'GOG'), ('rockstar', 'Rockstar Social Club'), ('drmfree', 'DRM free'), ('bethesda', 'Bethesda.net Launcher'), ('squareenix', 'Square Enix Launcher'), ('epic', 'Epic Game Store'), ('playstation', 'Playstation Store'), ('xbox', 'Xbox Store'), ('xboxplayanywhere', 'Xbox Play Anywhere'), ('nintendo', 'Nintendo eStore'), ('n/a', 'Not available')], default='n/a', max_length=25)),
                ('region', models.CharField(choices=[('steam', 'Steam'), ('origin', 'Origin'), ('uplay', 'UPlay'), ('battlenet', 'Battle.net'), ('windowsstore', 'Windows Store'), ('gog', 'GOG'), ('rockstar', 'Rockstar Social Club'), ('drmfree', 'DRM free'), ('bethesda', 'Bethesda.net Launcher'), ('squareenix', 'Square Enix Launcher'), ('epic', 'Epic Game Store'), ('playstation', 'Playstation Store'), ('xbox', 'Xbox Store'), ('xboxplayanywhere', 'Xbox Play Anywhere'), ('nintendo', 'Nintendo eStore'), ('n/a', 'Not available')], default='n/a', max_length=25)),
            ],
        ),
        migrations.CreateModel(
            name='Shop',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('status', models.CharField(choices=[('active', 'Active'), ('inactive', 'Inactive')], default='inactive', max_length=25)),
                ('name', models.CharField(max_length=255)),
                ('logo', models.ImageField(blank=True, max_length=550, null=True, upload_to='logos')),
                ('shop_url', models.URLField(blank=True, max_length=550, null=True)),
                ('affiliate_url', models.URLField(blank=True, max_length=550, null=True)),
                ('partner_link', models.CharField(blank=True, max_length=255, null=True)),
                ('trustpilot_url', models.URLField(blank=True, max_length=550, null=True)),
                ('trustpilot_rating', models.IntegerField(blank=True, null=True)),
            ],
        ),
        migrations.RenameModel(
            old_name='Screenshots',
            new_name='Screenshot',
        ),
        migrations.RenameField(
            model_name='screenshot',
            old_name='info_image',
            new_name='image',
        ),
        migrations.CreateModel(
            name='Price',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('currency', models.CharField(choices=[('eur', 'Euro'), ('usd', 'US Dollar'), ('gbp', 'Pound sterling'), ('n/a', 'Not available')], default='n/a', max_length=3)),
                ('amount', models.DecimalField(decimal_places=2, default=0.0, max_digits=5)),
                ('fk_deal', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gamedata.Deal')),
            ],
        ),
        migrations.CreateModel(
            name='Info',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('source', models.CharField(choices=[('steamstore', 'Steam store'), ('steamapi', 'Steam api'), ('igdb', 'IGDB.com'), ('n/a', 'Not available')], default='n/a', max_length=25)),
                ('source_id', models.CharField(max_length=255)),
                ('api_url', models.URLField(blank=True, max_length=550, null=True)),
                ('dyn_json', django.contrib.postgres.fields.jsonb.JSONField()),
                ('fk_game', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gamedata.Game')),
            ],
        ),
        migrations.AddField(
            model_name='deal',
            name='fk_shop',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='gamedata.Shop'),
        ),
    ]
