# Generated by Django 3.0.5 on 2020-07-08 14:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gamedata', '0014_game_cleaned_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='deal',
            name='cleaned_name',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
