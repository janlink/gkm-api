# Generated by Django 3.0.5 on 2020-07-08 12:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gamedata', '0011_auto_20200708_1450'),
    ]

    operations = [
        migrations.AddField(
            model_name='deal',
            name='url',
            field=models.URLField(blank=True, max_length=550, null=True),
        ),
    ]
