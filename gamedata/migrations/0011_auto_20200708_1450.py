# Generated by Django 3.0.5 on 2020-07-08 12:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gamedata', '0010_auto_20200626_1447'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deal',
            name='fk_game',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='gamedata.Game'),
        ),
        migrations.AlterField(
            model_name='deal',
            name='region',
            field=models.CharField(choices=[('ger', 'Germany'), ('eu', 'Europe'), ('uk', 'United Kingdom'), ('us', 'United States'), ('ru', 'Russia'), ('pl', 'Poland'), ('apac', 'Asia/Pacific'), ('emea', 'Europe, Middle East and Africa'), ('latam', 'Latin America'), ('world', 'Worldwide'), ('n/a', 'Not available')], default='n/a', max_length=25),
        ),
    ]
