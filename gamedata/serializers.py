from rest_framework import serializers
from gamedata.models import Game, GameDescription, Screenshot, Shop, Deal, Price, Info


class GameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = ['id', 'created', 'updated', 'name', 'slug', 'gtype', 'cover_horizontal', 'cover_vertical', 'artwork',
                  'release_date', 'developer', 'publisher', 'website', 'genre']


class GameDescriptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = GameDescription
        fields = ['id', 'fk_game', 'language', 'desc_short', 'desc_long']


class ScreenshotSerializer(serializers.ModelSerializer):
    class Meta:
        model = Screenshot
        fields = ['id', 'fk_game', 'image']


class ShopSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shop
        fields = ['id', 'created', 'updated', 'status', 'name', 'logo', 'shop_url',
                  'affiliate_url', 'partner_link', 'trustpilot_url', 'trustpilot_rating']


class DealSerializer(serializers.ModelSerializer):
    class Meta:
        model = Deal
        fields = ['id', 'fk_game', 'fk_shop', 'created',
                  'updated', 'url', 'name', 'status', 'plattform', 'region', 'is_checked']


class PriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Price
        fields = ['id', 'fk_deal', 'created', 'currency', 'amount']


class InfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Info
        fields = ['id', 'fk_game', 'created', 'updated',
                  'source', 'source_id', 'api_url', 'dyn_json']


class PriceInfoSerializer(serializers.Serializer):
    game_id = serializers.IntegerField()
    currency = serializers.CharField(max_length=3)
    min_price = serializers.DecimalField(max_digits=8, decimal_places=2,)
    max_price = serializers.DecimalField(max_digits=8, decimal_places=2,)
