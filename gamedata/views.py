from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework import filters
from django.db.models import Min, Max, F, Q
from gamedata.serializers import GameSerializer, GameDescriptionSerializer, ScreenshotSerializer, ShopSerializer, DealSerializer, PriceSerializer, InfoSerializer, PriceInfoSerializer
from gamedata.models import Game, GameDescription, Screenshot, Shop, Deal, Price, Info


class GameViewSet(viewsets.ModelViewSet):
    """
    API endpoint returns all games.
    """
    queryset = Game.objects.all()
    serializer_class = GameSerializer
    permission_classes = [permissions.AllowAny]
    # ordering_fields = ['id', 'name']

class GameDescriptionViewSet(viewsets.ModelViewSet):
    """
    API endpoint returns all game descriptions.
    """
    queryset = GameDescription.objects.all()
    serializer_class = GameDescriptionSerializer
    permission_classes = [permissions.AllowAny]
    filterset_fields = ['fk_game', 'language']

class ScreenshotViewSet(viewsets.ModelViewSet):
    """
    API endpoint returns all screenshots.
    """
    queryset = Screenshot.objects.all()
    serializer_class = ScreenshotSerializer
    permission_classes = [permissions.AllowAny]
    filterset_fields = ['fk_game']

class ShopViewSet(viewsets.ModelViewSet):
    """
    API endpoint returns all shops.
    """
    queryset = Shop.objects.all()
    serializer_class = ShopSerializer
    permission_classes = [permissions.AllowAny]

class DealViewSet(viewsets.ModelViewSet):
    """
    API endpoint returns all deals.
    """
    serializer_class = DealSerializer
    permission_classes = [permissions.AllowAny]
    def get_queryset(self):
        """
        Prices can be filtered after game_id and currency
        """
        queryset = Deal.objects.all()
        fk_game = self.request.query_params.get('fk_game', None)
        if fk_game is not None:
            queryset = queryset.filter(fk_game=fk_game)

        return queryset

class PriceViewSet(viewsets.ModelViewSet):
    """
    API endpoint returns all prices.
    """
    serializer_class = PriceSerializer
    permission_classes = [permissions.AllowAny]
    def get_queryset(self):
        """
        Prices can be filtered after game_id and currency
        """
        queryset = Price.objects.all()

        fk_deal = self.request.query_params.get('fk_deal', None)
        if fk_deal is not None:
            queryset = queryset.filter(fk_deal=fk_deal)
        
        deal_list = self.request.query_params.get('deal_list', None)
        if deal_list is not None:
            queryset = queryset.filter(fk_deal__in=eval(deal_list))

        currency = self.request.query_params.get('currency', None)
        if currency is not None:
            queryset = queryset.filter(currency=currency)

        max_created = queryset\
          .values('fk_deal', 'currency')\
          .annotate(max_created=Max('created'))\
          .order_by()

        q_statement = Q()
        for item in max_created:
          q_statement |= (Q(fk_deal__exact=item['fk_deal']) & Q(currency__exact=item['currency']) & Q(created=item['max_created']))

        queryset = queryset.filter(q_statement)
        return queryset

class InfoViewSet(viewsets.ModelViewSet):
    """
    API endpoint returns all infos.
    """
    queryset = Info.objects.all()
    serializer_class = InfoSerializer
    permission_classes = [permissions.AllowAny]
    filterset_fields = ['fk_game', 'source', 'source_id']

class PriceInfoViewSet(viewsets.ModelViewSet):
    """
    API endpoint returns all min and max price per game & currency.
    """
    serializer_class = PriceInfoSerializer
    permission_classes = [permissions.AllowAny]

    def get_queryset(self):
        """
        Prices can be filtered after game_id and currency
        """
        queryset = Price.objects\
            .select_related('fk_deal')\
            .select_related('fk_deal__fk_game')\
            .annotate(game_id=F('fk_deal__fk_game'))\
            .values('game_id', 'currency')\
            .exclude(amount=0.0)\
            .annotate(min_price=Min('amount'))\
            .annotate(max_price=Max('amount'))

        game_id = self.request.query_params.get('game_id', None)
        if game_id is not None:
            queryset = queryset.filter(game_id=game_id)
        
        currency = self.request.query_params.get('currency', None)
        if currency is not None:
            queryset = queryset.filter(currency=currency)

        return queryset
