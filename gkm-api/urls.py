"""gkm-api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static
from rest_framework import routers
import gamedata.views
import staging.views
import stats.views

router = routers.DefaultRouter()
router.register(r'games', gamedata.views.GameViewSet)
router.register(r'gamedescriptions', gamedata.views.GameDescriptionViewSet)
router.register(r'screenshots', gamedata.views.ScreenshotViewSet)
router.register(r'shops', gamedata.views.ShopViewSet)
router.register(r'deals', gamedata.views.DealViewSet, basename='deals')
router.register(r'prices', gamedata.views.PriceViewSet, basename='prices')
router.register(r'infos', gamedata.views.InfoViewSet)
router.register(r'priceinfo', gamedata.views.PriceInfoViewSet,
                basename='priceinfo')

router.register(r'staging/igdb/genres',
                staging.views.IgdbGenreViewSet, basename='genres')
router.register(r'staging/igdb/involvedCompanies',
                staging.views.IgdbInvolvedCompaniesViewSet, basename='involvedCompanies')
router.register(r'staging/igdb/artworks',
                staging.views.IgdbArtworkViewSet, basename='artworks')
router.register(r'staging/igdb/screenshots',
                staging.views.IgdbScreenshotViewSet, basename='screenshots')
router.register(r'staging/igdb/cover',
                staging.views.IgdbCoverViewSet, basename='cover')
router.register(r'staging/igdb/externalGames',
                staging.views.IgdbExternalGameViewSet, basename='externalGames')
router.register(r'staging/igdb/games',
                staging.views.IgdbGameViewSet, basename='games')
router.register(r'staging/igdb/companies',
                staging.views.IgdbCompanyViewSet, basename='companies')
router.register(r'staging/igdb/gameVideos',
                staging.views.IgdbVideoViewSet, basename='gameVideos')
router.register(r'staging/steam/games',
                staging.views.SteamGameViewSet, basename='games')

router.register(r'stats/chartPositions',
                stats.views.ChartPosistionViewSet)
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
